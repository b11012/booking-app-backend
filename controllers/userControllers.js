// DEPENDENCIES

const User = require('../models/User');
const bcrypt = require('bcryptjs');
const auth = require('../auth')
const Product = require('../models/Product');

// USER REGISTRATION
module.exports.userRegistration = (req, res) => {

	console.log(req.body);

	User.findOne({email: req.body.email}).then(result => {

		console.log(result);

	
	const hashedPW = bcrypt.hashSync(req.body.password, 10)
			

		if(result !== null && result.email === req.body.email){
			return res.send('Email is already taken')

		} else {

			let newUser = new User({
				firstName: req.body.firstName,
				lastName: req.body.lastName,
				address: req.body.address,
				mobileNo: req.body.mobileNo,
				email: req.body.email,
				password: hashedPW
			})

			newUser.save()
			.then(result => res.send(result))
			.catch(error => res.send(error));
		}
	})
	.catch(error => res.send(error));
};

// GET ALL USERS
module.exports.showAllUsers = (req, res) => {

	User.find({})
	.then(result => res.send(result))
	.catch(err => res.send(err));
};

// LOGIN USER

module.exports.userLogin = (req, res) => {

	console.log(req.body);

	User.findOne({email: req.body.email})
	.then(foundUser => {

		if(foundUser === null){
			return res.send(false);

		} else {

			const isPasswordCorrect = bcrypt.compareSync(req.body.password, foundUser.password)

			req.body.password === foundUser.password
			console.log(isPasswordCorrect);

			if(isPasswordCorrect) {

				return res.send({accessToken: auth.createAccessToken(foundUser)})

			} else {

				return res.send(false)
			}
		}
	})
	.catch(err => res.send(err));
}

// RETRIEVE SINGLE USER DETAILS

module.exports.userTokenDetails = (req, res) => {

	console.log(req.user);

	User.findById(req.user.id)
	.then(result => res.send(result))
	.catch(err => res.send(err))
};

// RETRIEVE SINGLE USER

module.exports.getUserDetails = (req, res) => {

	console.log(req.user);
	/*
	expected output: decoded token
		{
		  id: '6296fea12e45745b86178021',
		  email: 'callMeLisa@gmail.com',
		  isAdmin: false,
		  iat: 1654136799
		}

	*/

	// find the logged in user's document from our db and send it to the client by its id

	User.findById(req.user.id)
	.then(result => res.send(result))
	.catch(err => res.send(err));
};

// RETRIEVE SINGLE USER BY ID

module.exports.getSingleUserController = (req, res) => {

	console.log(req.params);

	User.findById(req.params.id)
	.then(result => res.send(result))
	.catch(error => res.send(error))
};


// CHECK EMAIL EXISTS
module.exports.checkEmailExists = (req,res) => {

	//console.log(req.body.email);//check if you can receive the email from our client's request body.
	
	//You can use find({email: req.body.email}), however, find() returns multiple documents in an ARRAY.

	User.findOne({email: req.body.email})
	.then(result => {

		//console.log(result)

		if(result === null){
			return res.send(false);
		} else {
			return res.send(true)
		}

	})
	.catch(err => res.send(err));
}

// UPDATE TO ADMIN

module.exports.updateTurnAdmin = (req,res) => {

	console.log(req.user.id);

	console.log(req.params.id);

	let updates = {

		isAdmin: true
	}

	User.findByIdAndUpdate(req.params.id, updates, {new: true})
	.then(updatedUser => res.send(updatedUser))
	.catch(err => res.send(err))
}

// UPDATE ADMIN TO FALSE

module.exports.falseAdmin = (req,res) => {

	console.log(req.user.id);

	console.log(req.params.id);

	let falseUpdates = {

		isAdmin: false
	}

	User.findByIdAndUpdate(req.params.id, falseUpdates, {new: true})
	.then(updatedUser => res.send(updatedUser))
	.catch(err => res.send(err))
}

// ORDER

module.exports.ordered = async (req, res) => {

	console.log(req.user.id)
	console.log(req.body.productId)

	if(req.user.isAdmin){
		return res.send(false)
	}

	let isUserUpdated = await User.findById(req.user.id).then(user => {

		console.log(user)

		let newProduct = {
			productId: req.body.productId,
			totalAmount: req.body.totalAmount
		}

		user.order.push(newProduct)

		return user.save()
		.then(user => true)
		.catch(err => err.message)
	})

	if(isUserUpdated !== true){
		return res.send({message: isUserUpdated})
	}

	if(isUserUpdated === true){
		return res.send(true)
	}
}

// GET ORDER

module.exports.getOrder = (req, res) => {

	User.findById(req.user.id)
	.then(result => res.send(result.order))
	.catch(err => res.send(err))
};
