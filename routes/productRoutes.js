// [SECTION] DEPENDENCIES 
const express = require("express");
const router = express.Router();

// [SECTION] IMPORTED MODULES
const productControllers = require("../controllers/productControllers");
const auth = require('../auth');

const {verify, verifyAdmin} = auth;


//create/add course
router.post('/', verify, verifyAdmin, productControllers.addProduct);

//get all courses
router.get('/', productControllers.getAllProduct);

//get single course
router.get('/getSingleProduct/:id',productControllers.getSingleProduct);

// update a course
router.put('/:id', verify, verifyAdmin, productControllers.updateProduct);
/*
	>> Try the route in Postman
	>> Send your updated course in Hangouts
*/

//archive
router.put('/archive/:id',verify, verifyAdmin,productControllers.archive);

//activate
router.put('/activate/:id',verify, verifyAdmin,productControllers.activate);

//get active courses
router.get('/getActiveProducts',productControllers.getActiveProduct);

// find courses by name
router.post('/findProductsByName', productControllers.findProductByName);

module.exports = router;