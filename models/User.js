const mongoose = require('mongoose');

let userSchema = new mongoose.Schema({

	firstName: {
		type: String,
		required: [true, "First Name is required"]
	},

	lastName: {
		type: String,
		required: [true, "Last Name is required"]
	},

	address: {
	  type: String,
	  required: [true, "Address is required"]
	},

	mobileNo: {
		type: String,
		required: [true, "Mobile Number is required"]
	},

	email: {
		type: String,
		required: [true, "Email is required"]

	},

	password: {
		type: String,
		required: [true, "Password is required"]
	},

	isAdmin: {
		type: Boolean,
		default: false
	},

	order: [
		{
			productId: {
				type: String,
				required: [true, "Product Id is required"]
			},
				
			totalAmount: {
				type: Number,
				required: [true, "Total amount is required"]
			},

			purchasedOn: {
				type: Date,
				default: new Date()
			}
					
		}
	]

})

module.exports = mongoose.model("User", userSchema);